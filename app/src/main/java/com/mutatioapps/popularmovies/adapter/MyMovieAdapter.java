package com.mutatioapps.popularmovies.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mutatioapps.popularmovies.R;
import com.mutatioapps.popularmovies.model.MovieResponse;
import com.mutatioapps.popularmovies.model.Result;

import java.util.List;

/**
 * Created by hp on 7/28/2017.
 */

public class MyMovieAdapter extends RecyclerView.Adapter<MyMovieAdapter.MyHolder>{

    List<Result> movies;
    Context context;

    public MyMovieAdapter(Context context, List<Result> movies){
        this.movies = movies;
        this.context = context;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_movie, parent , false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
       Result perMovie = movies.get(position);
        holder.movieName.setText(perMovie.getTitle());
        holder.movieRating.setText("Rating: "+perMovie.getVoteAverage()+"/10"); //'cauz we want to show it out of 10;
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyHolder extends RecyclerView.ViewHolder{

        public TextView movieName;
        public TextView movieRating;

        public MyHolder(View item){
            super(item);

            movieName = (TextView)item.findViewById(R.id.tv_movie_name);
            movieRating = (TextView)item.findViewById(R.id.tv_movie_rating);
        }

    }
}
