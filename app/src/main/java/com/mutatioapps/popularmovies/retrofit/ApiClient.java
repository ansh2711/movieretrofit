package com.mutatioapps.popularmovies.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hp on 7/28/2017.
 */

public class ApiClient {

    private static Retrofit retrofit = null;
    private static String BASE_URL = "http://api.themoviedb.org/3/"; //note it should awals end with `/`

    public static Retrofit getClient(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
