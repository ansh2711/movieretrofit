package com.mutatioapps.popularmovies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.mutatioapps.popularmovies.adapter.MyMovieAdapter;
import com.mutatioapps.popularmovies.model.MovieResponse;
import com.mutatioapps.popularmovies.model.Result;
import com.mutatioapps.popularmovies.retrofit.ApiClient;
import com.mutatioapps.popularmovies.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    RecyclerView recyclerView;
    LinearLayoutManager mLinearLayoutManager;
    MyMovieAdapter myMovieAdapter;
    List<Result> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar)findViewById(R.id.progress);
        recyclerView = (RecyclerView)findViewById(R.id.movieList);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(mLinearLayoutManager);

        movies = new ArrayList<>();
        myMovieAdapter = new MyMovieAdapter(this, movies);
        recyclerView.setAdapter(myMovieAdapter);

        showProgress(true);
        fetchMovieData();
    }

    public void showProgress(boolean value){
        progressBar.setVisibility(value ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(value ? View.GONE : View.VISIBLE);
    }

    public void fetchMovieData(){

        String apiKey = "8f5f6df0585d0d2b95c786ab35858e78";

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class); //creating your apiClient
        Call<MovieResponse> movieResponseCall = apiInterface.getPopularMovies(apiKey); //creating a request
        //now make a call using retrofit
        movieResponseCall.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                showProgress(false);
                //this callback method is executed when a response is received from the api.
                if(response.body() != null){
                    movies = response.body().getResults(); //here movies will be deserialized automatically using GSON as we have added it to our http retrofit client.
                    myMovieAdapter.notifyDataSetChanged(); //notifying changes to adapter
                    Log.d("Retrofit","got a response");
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                showProgress(false);
                //this callback method is executed when no response is received from the api.
                Log.d("Retrofit","failed to get a response");
            }
        });

    }
}
